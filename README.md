## qssi-user 12
12 SKQ1.211019.001 V14.0.1.0.SJGMIXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: surya
- Brand: POCO
- Flavor: qssi-user
- Release Version: 12
12
- Kernel Version: 4.14.190
- Id: SKQ1.211019.001
- Incremental: V14.0.1.0.SJGMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/surya_global/surya:12/RKQ1.211019.001/V14.0.1.0.SJGMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211019.001-V14.0.1.0.SJGMIXM-release-keys
- Repo: poco/surya
